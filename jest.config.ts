import type {Config} from '@jest/types'
import path from 'path'

const config: Config.InitialOptions = {
  verbose: true,
  preset: 'react-native',
  testMatch: ['**/*.test.ts?(x)'],
  setupFiles: [
    './jest-setup.ts',
    './node_modules/react-native-gesture-handler/jestSetup.js',
  ],
  transformIgnorePatterns: ['!node_modules/react-runtime'],
  moduleNameMapper: {
    '#src': path.resolve(`${__dirname}/src`),
    '#classes': path.resolve(`${__dirname}/src/classes`),
    '#screens': path.resolve(`${__dirname}/src/screens`),
    '#navigation': path.resolve(`${__dirname}/src/navigation`),
    '#components': path.resolve(`${__dirname}/src/components`),
    '#styles': path.resolve(`${__dirname}/src/styles`),
    '#redux': path.resolve(`${__dirname}/src/redux`),
    '#jest-utils': path.resolve(`${__dirname}/jest-utils`),
  },
  moduleFileExtensions: ['js', 'ts', 'tsx', 'json', 'node'],
  collectCoverageFrom: ['src/**/*.ts', 'src/**/*.tsx'],
}
export default config
