import 'react-native'
import React, {ReactElement} from 'react'
import renderer, {
  act,
  TestRendererOptions,
  ReactTestRenderer,
} from 'react-test-renderer'
import configureStore from 'redux-mock-store'
import {Provider as ReduxProvider} from 'react-redux'
import thunk from 'redux-thunk'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

/**
 * Render with store
 * @param children
 * @param options
 * @returns ReactTestRenderer
 */
export const renderWithStore = async (
  initialStoreState: redux.RootState,
  children: ReactElement,
  options?: TestRendererOptions,
): Promise<ReactTestRenderer> => {
  let result: ReactTestRenderer | undefined
  await act(async () => {
    result = await renderer.create(
      <ReduxProvider store={mockStore(initialStoreState)}>
        {children}
      </ReduxProvider>,
      options,
    )
  })

  if (result === undefined) {
    throw Error(
      'CRITICAL_ERROR: this error is here only to not return result possibly as undefined',
    )
  }

  return result
}

/**
 * Render without store
 * @param children
 * @param options
 * @returns ReactTestRenderer
 */
export const render = async (
  children: ReactElement,
  options?: TestRendererOptions,
): Promise<ReactTestRenderer> => {
  let result: ReactTestRenderer | undefined
  await act(async () => {
    result = await renderer.create(children, options)
  })

  if (result === undefined) {
    throw Error(
      'CRITICAL_ERROR: this error is here only to not return result possibly as undefined',
    )
  }

  return result
}
export {act} from 'react-test-renderer'
