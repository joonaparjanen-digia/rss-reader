import {ReactTestInstance} from 'react-test-renderer'
import {NavParamList} from '#types'

type Screens = typeof import('#screens')

/**
 * You will have to provide name for screen name as generic type
 * if ```NavParamList``` doesn't have nav route named after it.
 * @param root
 * @param name
 * @returns
 */
export function findScreenInstanceProps<T extends keyof Screens>(
  root: ReactTestInstance,
  name: T | keyof NavParamList,
): Screens[T]['defaultProps'] {
  const instance = root.findByProps({name})
  return instance?.props?.children?.props
}
