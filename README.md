# First Steps to get it running 

Do these steps to get it running: 
 - 1. Install Visual Studio Code: https://code.visualstudio.com/download
 - 2. Setup environment for react native (React Native CLI Quickstart): https://reactnative.dev/docs/environment-setup
 - 3. cd to project folder where this README.md is
 - 4. `npm i`
 - 5. `npm run start`
 - 6. `npm run android` or `npm run ios` 
 - 7. Enjoy the app on your own phone or phone simulator :)

Try to get it running before we start the workshop. 
If you have some challenges on the way, ask me what to do!  

# Why I use these technologies with React-Native 

## TypeScript
New projects should be started with TypeScript instead of JavaScript. This is a very bold statement. 
It's used on basically every big public module that is on the npm-library. TypeScript is used by Microsoft, Airbnb, Asana, Lyft, Slack, all Angular 2+ developers, multiple React & Vue.js developers, and thousands of other companies. 

### Power of TypeScript comes from these things: 
- Your own code is more readable for other team members _(and yourself, after you have forgotten how you did it)_. 
- You can easily use unknown library without documentation, just by using their type definitions and IntelliSense. 
- TypeScript forces you to write better code. 
- It's easier to refactor code without breaking it significantly. _(when editing types, you can easily test that the changes are implemented correctly on everywhere inside the project)_
- You can still do JS inside TypeScript, **but it is strongly discouraged**. _(Almost always when you _"need"_ to not use strictly typed variables, the implementation can be done better with some other way.)_ 
- Forces team to develop an app in the similar way, and reduces chances for devs to use _"ninja"_-code.
- Project has complete type-safety that can be easily tested.

### Often used arguments against TypeScript:
- **Increases the duration of on-boarding new developers:** <br>
No! TS helps new developers to join, because it let's everyone understand unknown code-base better by exposing how the functionality has been implemented.<br>
**It exposes these things for example _(you need to use IntelliSense)_**:
    - What can be imported from import statement
    - What is the shape of params that needs to go inside function
    - What is the shape of output of the function 
    - Etc.

- **Complicates maintenance:** <br>
No! It can be done exactly as needed when TS has been done right, and the new implementation can be easily tested.  

- **Introduces a lot of conflicts with React:** <br>
No! But it might seem so if you are beginner at TS, because React does need some skill with TS before you can use it more easily. But after everything clicks for you, there is no turning back...     

- **Increases development time:** <br>
No! Development time is reduced, because need for documentation is smaller. Because by default with TS everyone in the team is in better understanding of other peoples code. _(And your own code that you did long time ago)_

- **Prevents recruiting good JS people:** <br>
No! Good JS-dev is also good TS-dev, TS is just JS with types.  

- **Make it impossible to bring code from non-TS codebases:** <br>
No! You can use non-TS codebases as is, or define your own types on top of it. 

## Redux _(with middleware)_

It is no wonder why `redux` have more than 3.5M downloads a week. 
I would strongly discourage anyone to be not using it for normal React or React Native application.

### Primary reasons for Redux: 
- Main function of Redux is to inform whole JSX update tree of changes that have happened inside application and re-render views if they are using specific data that is changing. 
- Standardises how data is stored and used from memory
- Makes testing more straight forward. 
- In React-like applications: 
    - It allows data to flow on every direction of the app, not just downward.
    - It is not adviced to trigger async functions inside views, because handling those events inside view might introduce racing conditions that might cause memory leaks, _(ie. when focus is lost inside view before the promise is resolved)_   
 
Even though it might seem too much boilerplate _(and even more so for using it with TS)_, it pays back the pain of setting that up by making unified way to update and read data for the application. So it makes it more  communication between devs 

## React Navigation 5

This is most widely used React Native library for navigating between different screens. It is really fast to set up, and it does the job really well and customizability is good. With React Navigation you get all the most basic tools for nawigation, Tab navigation, Stack navigation and drawer navigation. You can even extend their own navigators to your own needs to get amazing custom transitions, but this is basically 0% documented so good luck _":D"_. _(I can maybe do example how to do it, if anyone is interested)_. But you can do basically 100% of the stuff you ever need to do with the stuff in the library. Extending navigators is just for making the look seem more unique.

### What to remember when using React Navigation

- There is 3 primary types of navigators: 
    - `Drawer`-navigator: View slides from the side, and can be accessed with native triggers.  
    - `Stack`-navigator: View cannot be changed with swiping. You have to programmatically change the screen.
    Generates header for the view.   
    - `Tab`-navigator: You can use gestures, swipe or tabs to move between screens.
- Every screen has `navigation` and `route` parameter.    
    - `navigation`: used for controlling the look or triggering navigation changes. 
        You can also use `useNavigation` to get reference to closest parent navigator, but that does not have native type-support, 
        so you should extend that hook by adding correct types to it so type-safety is assured. 
    - `route`: used for accessing data that is forwarded to the screen from the navigator.

