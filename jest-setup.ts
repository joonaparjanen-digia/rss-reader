/* eslint-disable no-undef */
import 'react-native-gesture-handler/jestSetup'

import './__mocks__'

jest.useFakeTimers()
jest.mock('react-native-reanimated', () => {
  return require('react-native-reanimated/mock')
})
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper')
