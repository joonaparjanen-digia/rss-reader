import {Article} from '#types'
import {XMLContext} from './XMLContext'

export interface IRssFeed {
  articleData: Record<string, Article>
  articleIds: string[]
}

export class RssFeed implements IRssFeed {
  readonly articleData: Record<string, Article>
  readonly articleIds: string[]

  constructor(props: IRssFeed) {
    this.articleData = props.articleData
    this.articleIds = props.articleIds
  }

  /* istanbul ignore next */
  static async fetchContext(url: string): Promise<XMLContext> {
    return await XMLContext.create(url)
  }

  static async fetch(url: string): Promise<RssFeed> {
    const context = await RssFeed.fetchContext(url)
    const articleRefs = context.selectMany('rss > channel > item')

    const articleData: Record<string, Article> = {}

    const articleIds = articleRefs.map(ref => {
      const guid = ref.find('guid').text()

      const article: Article = {
        imageUrl: ref
          .find('enclosure')
          .attr('url')
          ?.replace(/w_205,h_115/, 'w_410,h_300'),
        link: ref.find('link').text(),
        title: ref.find('title').text(),
      }

      articleData[guid] = article
      return guid
    })

    return new RssFeed({
      articleIds,
      articleData,
    })
  }
}
