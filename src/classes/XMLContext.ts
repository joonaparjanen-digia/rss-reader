import ch, {Cheerio, CheerioAPI, Node} from 'cheerio'
import axios from 'axios'

export class XMLContext {
  readonly $: CheerioAPI

  constructor($: CheerioAPI) {
    this.$ = $
  }

  public selectMany(selector: string) {
    const matches = this.$(selector)
    const selections: Cheerio<Node>[] = []

    matches.map((_id, el) => {
      let node = this.$(el)
      selections.push(node)
    })
    return selections
  }

  private static async requestXml(url: string): Promise<CheerioAPI> {
    const {data} = await axios.get(url)
    return ch.load(data, {
      xmlMode: true,
      lowerCaseTags: true,
    })
  }

  public static async create(uri: string): Promise<XMLContext> {
    const $ = await this.requestXml(uri)
    return new XMLContext($)
  }
}
