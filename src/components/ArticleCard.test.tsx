import React from 'react'
import {act, renderWithStore} from '#jest-utils'
import ArticleCard from './ArticleCard'

const INITIAL_STORE_STATE: redux.RootState = {
  rssFeed: {
    articleData: {
      '<guid>': {
        link: '<link>',
        title: '<title>',
        imageUrl:
          'https://images.cdn.yle.fi/image/upload//w_205,h_115,q_70/39-81157060ae608db538f.jpg',
      },
    },
    feeds: {},
  },
}

describe('ArticleCard', () => {
  it('renders correctly & is clickable', async () => {
    const onPressEvent = jest.fn()
    const {root} = await renderWithStore(
      INITIAL_STORE_STATE,
      <ArticleCard articleGuid="<guid>" onPress={onPressEvent} />,
    )
    const btns = root.findAllByProps(
      {testID: 'ArticleCard.button'},
      {deep: false},
    )

    expect(btns.length).toBe(1)

    act(() => btns[0].props.onPress())

    expect(onPressEvent.mock.calls.length).toBe(1)
    expect(onPressEvent.mock.calls[0][0]).toBe('<link>')
  })
})
