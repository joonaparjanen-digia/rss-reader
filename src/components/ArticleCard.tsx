import React, {useCallback} from 'react'
import {View, ImageBackground, Text, StyleSheet} from 'react-native'
import {TouchableHighlight} from 'react-native-gesture-handler'
import LinearGradient from 'react-native-linear-gradient'

import {styles} from '#styles'
import {useArticle} from '#redux'

type Props = {
  articleGuid: string
  onPress: (artcleUrl: string) => void
}
const ArticleCard: React.FC<Props> = ({articleGuid, onPress}) => {
  const article = useArticle(articleGuid)

  const onPressBase = useCallback(() => {
    onPress(article.link)
  }, [article, onPress])

  return (
    <TouchableHighlight
      style={localStyles.container}
      onPress={onPressBase}
      testID="ArticleCard.button">
      <View>
        <ImageBackground
          style={[styles.container.card, localStyles.imageContainer]}
          imageStyle={styles.container.card}
          source={{
            uri: article.imageUrl,
          }}>
          <LinearGradient
            start={{x: 0.5, y: 0}}
            end={{x: 0.5, y: 1}}
            colors={[
              '#00000000',
              '#00000007',
              '#00000025',
              '#00000055',
              '#00000090',
              '#000000bb',
            ]}
            style={[styles.container.card, localStyles.linearGradient]}
          />
          <Text style={[styles.text.cardTitle]}>{article.title}</Text>
        </ImageBackground>
      </View>
    </TouchableHighlight>
  )
}

const localStyles = StyleSheet.create({
  container: {
    margin: 2,
    borderRadius: 22,
  },
  linearGradient: {
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    position: 'absolute',
  },
  imageContainer: {
    height: 150,
    margin: 4,
    elevation: 3,
    backgroundColor: '#F4D1AE',
    justifyContent: 'flex-end',
  },
})

export default React.memo(ArticleCard)
