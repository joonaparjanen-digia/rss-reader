import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'

import {ArticleWebViewScreen} from '#screens'
import {RootNavParamList} from '#types'

import RssFeedNavigator from './RssFeedNav'

const RootStack = createStackNavigator<RootNavParamList>()
const RootNavigator: React.FC = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={{
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          headerTintColor: '#06BCC1',
          headerStyle: {
            backgroundColor: '#12263A',
          },
        }}>
        <RootStack.Screen name="RssFeedNav" component={RssFeedNavigator} />
        <RootStack.Screen
          name="ArticleWebViewScreen"
          component={ArticleWebViewScreen}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  )
}

export default RootNavigator
