import React, {useLayoutEffect} from 'react'
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs'
import {RssFeedScreen} from '#screens'
import {StackScreenProps, RssFeedNavParamList} from '#types'

const RssFeedStack = createMaterialTopTabNavigator<RssFeedNavParamList>()
const RssFeedNavigator: React.FC<StackScreenProps<'RssFeedNav'>> = ({
  navigation,
}) => {
  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'RSS Feed',
    })
  }, [navigation])

  return (
    <RssFeedStack.Navigator
      tabBarOptions={{
        labelStyle: {
          fontSize: 14,
          fontWeight: 'bold',
          textTransform: 'capitalize',
          color: '#12263A',
        },
        style: {
          backgroundColor: '#F4EDEA',
          elevation: 3,
        },
        indicatorStyle: {
          height: 5,
          backgroundColor: '#06BCC1',
        },
      }}>
      <RssFeedStack.Screen
        name="Yle"
        component={RssFeedScreen}
        initialParams={{
          rssUrl:
            'https://feeds.yle.fi/uutiset/v1/majorHeadlines/YLE_UUTISET.rss',
        }}
      />
      <RssFeedStack.Screen
        name="IltaLehti"
        component={RssFeedScreen}
        initialParams={{
          rssUrl: 'https://www.iltalehti.fi/rss/uutiset.xml',
        }}
      />
      <RssFeedStack.Screen
        name="IltaSanomat"
        component={RssFeedScreen}
        initialParams={{
          rssUrl: 'https://www.is.fi/rss/tuoreimmat.xml',
        }}
      />
    </RssFeedStack.Navigator>
  )
}

export default RssFeedNavigator
