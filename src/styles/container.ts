import {StyleSheet} from 'react-native'

const container = StyleSheet.create({
  card: {
    borderRadius: 20,
  },
})

export default container
