import {StyleSheet} from 'react-native'

const text = StyleSheet.create({
  cardTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
    marginHorizontal: 8,
    marginVertical: 5,
  },
})

export default text
