import container from './container'
import text from './text'

export const styles = {
  container,
  text,
}
