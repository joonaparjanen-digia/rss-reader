import {RootNavParamList, RssFeedNavParamList} from './navigators'

export type NavParamListMap = {
  RootNav: RootNavParamList
  RssFeedNav: RssFeedNavParamList
}
export type NavParamList = RootNavParamList & RssFeedNavParamList
