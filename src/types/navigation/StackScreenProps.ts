import {RouteProp} from '@react-navigation/core'
import {StackNavigationProp} from '@react-navigation/stack'
import {NavParamList} from './NavParamList'

/**
 * @param T Screen's name in navigator, it should be enforced to be always unique.
 * When same name is "needed", prefix the name of the child with parents name
 */
export type StackScreenProps<T extends keyof NavParamList> = {
  navigation: StackNavigationProp<NavParamList, T>
  route: RouteProp<NavParamList, T>
}
