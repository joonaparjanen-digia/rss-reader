import {RouteProp} from '@react-navigation/core'
import {MaterialTopTabNavigationProp} from '@react-navigation/material-top-tabs'
import {NavParamListMap, NavParamList} from './NavParamList'

type ResolveRouteName<T extends keyof NavParamListMap | keyof NavParamList> =
  T extends keyof NavParamListMap
    ? keyof NavParamListMap[T]
    : T extends keyof NavParamList
    ? T
    : any // this never happens

/**
 * @param T TabScreen's or MaterialTopTabNavigator's name.
 */
export type MaterialTopTabScreenProps<
  T extends keyof NavParamListMap | keyof NavParamList,
  ScreenName extends ResolveRouteName<T> = ResolveRouteName<T>,
> = {
  navigation: MaterialTopTabNavigationProp<NavParamList, ScreenName>
  route: RouteProp<NavParamList, ScreenName>
}
