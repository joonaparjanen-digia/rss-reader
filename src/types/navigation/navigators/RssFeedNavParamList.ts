import {RssScreenProps as RssFeedScreenProps} from '../screens/RssFeedScreenProps'

export type RssFeedNavParamList = {
  Yle: RssFeedScreenProps
  IltaLehti: RssFeedScreenProps
  IltaSanomat: RssFeedScreenProps
}
