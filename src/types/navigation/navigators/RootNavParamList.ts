import {ParentNavigator} from '../ParentNavigator'
import {ArticleWebViewScreenProps} from '../screens'
import {RssFeedNavParamList} from './RssFeedNavParamList'

export type RootNavParamList = {
  RssFeedNav: ParentNavigator<RssFeedNavParamList>
  ArticleWebViewScreen: ArticleWebViewScreenProps
}
