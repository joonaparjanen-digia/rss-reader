export type ParentNavigator<ChildNavParamList> = {
  screen?: keyof ChildNavParamList
  params?: ChildNavParamList[keyof ChildNavParamList]
  initial?: boolean
}
