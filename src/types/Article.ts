export type Article = {
  title: string
  link: string
  imageUrl?: string
}
