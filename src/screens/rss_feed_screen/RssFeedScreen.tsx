import React, {useCallback, useEffect} from 'react'
import {View, FlatList, StyleSheet, ListRenderItemInfo} from 'react-native'
import {useDispatch} from 'react-redux'
import {MaterialTopTabScreenProps} from '#types'
import {ArticleCard} from '#components'
import {fetchYleRSSArticles, useFeedArticleIds, useFeedIsFetching} from '#redux'

const RssFeedScreen: React.FC<MaterialTopTabScreenProps<'RssFeedNav'>> = ({
  route,
  navigation,
}) => {
  const {rssUrl} = route.params

  const articleIds = useFeedArticleIds(rssUrl)
  const isFetching = useFeedIsFetching(rssUrl)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchYleRSSArticles(rssUrl))
  }, [dispatch, rssUrl])

  const onOpenArticleWebView = useCallback(
    (articleUrl: string) => {
      navigation.navigate('ArticleWebViewScreen', {articleUrl})
    },
    [navigation],
  )

  const renderArticle = useCallback(
    ({item: guid}: ListRenderItemInfo<string>) => {
      return <ArticleCard articleGuid={guid} onPress={onOpenArticleWebView} />
    },
    [onOpenArticleWebView],
  )

  const keyExtractor = useCallback((item: string, _index: number) => {
    return item
  }, [])

  return (
    <View style={styles.container}>
      <FlatList
        data={articleIds}
        style={styles.flatlistContainer}
        refreshing={isFetching}
        renderItem={renderArticle}
        keyExtractor={keyExtractor}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flatlistContainer: {
    flexGrow: 1,
    backgroundColor: '#F4EDEA',
  },
})

export default RssFeedScreen
