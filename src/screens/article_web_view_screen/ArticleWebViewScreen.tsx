import React, {useLayoutEffect} from 'react'
import {View, StyleSheet} from 'react-native'
import {WebView} from 'react-native-webview'

import {StackScreenProps} from '#types'

const ArticleWebViewScreen: React.FC<StackScreenProps<'ArticleWebViewScreen'>> =
  ({route, navigation}) => {
    const {articleUrl} = route.params

    useLayoutEffect(() => {
      navigation.setOptions({headerTitle: 'Article View'})
    })

    return (
      <View style={styles.container}>
        <WebView source={{uri: articleUrl}} scrollEnabled />
      </View>
    )
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default ArticleWebViewScreen
