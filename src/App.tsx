/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import {applyMiddleware, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'

import {rootReducer} from '#redux'
import {RootNav} from '#navigation'

const store = createStore(rootReducer, applyMiddleware(thunk))

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <RootNav />
    </Provider>
  )
}

export default App
