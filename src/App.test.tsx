import React from 'react'
import App from './App'
import {act, render, findScreenInstanceProps} from '#jest-utils'

describe('App', () => {
  /**
   * 1) Navigate to webview with ArticleCard
   * 2) Test that web view has correct link
   */
  it('render & navigate correctly', async () => {
    const renderer = await render(<App />)

    let articleBtns = renderer.root.findAllByProps({
      testID: 'ArticleCard.button',
    })

    act(articleBtns[0].props.onPress)
    await act(async () => renderer.update(<App />))

    let webViewProps = findScreenInstanceProps(
      renderer.root,
      'ArticleWebViewScreen',
    )

    expect(webViewProps?.route?.params.articleUrl).toBe('LINK_0')
  })
})
