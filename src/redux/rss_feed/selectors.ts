import {useMemo} from 'react'
import {useSelector} from 'react-redux'

const defaultRssFeed = {
  fetching: false,
  articleIds: [],
}

export function useFeed(rssUrl: string) {
  return useSelector(
    ({rssFeed}: redux.RootState) => rssFeed.feeds[rssUrl] ?? defaultRssFeed,
  )
}

export function useFeedArticleIds(rssUrl: string) {
  const feed = useFeed(rssUrl)

  return useMemo(() => feed.articleIds, [feed])
}

export function useFeedIsFetching(rssUrl: string) {
  const feed = useFeed(rssUrl)
  return useMemo(() => feed.fetching, [feed])
}

export function useArticle(guid: string) {
  return useSelector(({rssFeed}: redux.RootState) => rssFeed.articleData[guid])
}
