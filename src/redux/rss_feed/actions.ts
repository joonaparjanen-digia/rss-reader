import {Dispatch} from 'redux'
import {Article} from '#types'
import {RssFeed} from '#classes'

type RSSArticlesAction_Request = {
  type: 'request-rss-articles'
  rssUrl: string
}

type RSSArticlesAction_Receive = {
  type: 'receive-rss-articles'
  rssUrl: string
  articleIds: string[]
  articleData: Record<string, Article>
}

export function fetchYleRSSArticles(rssUrl: string) {
  return async (dispatch: Dispatch, _storeState: redux.RootState) => {
    dispatch<RSSArticlesAction_Request>({
      type: 'request-rss-articles',
      rssUrl,
    })

    const feed = await RssFeed.fetch(rssUrl)

    dispatch<RSSArticlesAction_Receive>({
      type: 'receive-rss-articles',
      rssUrl,
      articleData: feed.articleData,
      articleIds: feed.articleIds,
    })
  }
}

export type RSSFeedAction =
  | RSSArticlesAction_Request
  | RSSArticlesAction_Receive
