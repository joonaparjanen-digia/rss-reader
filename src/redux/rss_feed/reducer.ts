import {union} from 'lodash'

import {Article} from '#types'

import {RSSFeedAction} from './actions'

export type RSSStoreState = {
  articleData: Record<string, Article>
  feeds: {
    [rssUrl in string]?: {
      fetching: boolean
      articleIds: string[]
    }
  }
}

const initialState: RSSStoreState = {
  articleData: {},
  feeds: {},
}

export function rssFeedReducer(
  state: RSSStoreState = initialState,
  action: RSSFeedAction,
): RSSStoreState {
  switch (action.type) {
    case 'request-rss-articles':
      return {
        ...state,
        feeds: {
          ...state.feeds,
          [action.rssUrl]: {
            articleIds: [],
            ...state.feeds[action.rssUrl],
            fetching: true,
          },
        },
      }

    case 'receive-rss-articles':
      return {
        ...state,
        articleData: {
          ...state.articleData,
          ...action.articleData,
        },
        feeds: {
          ...state.feeds,
          [action.rssUrl]: {
            fetching: false,
            articleIds: union(
              state.feeds[action.rssUrl]?.articleIds,
              action.articleIds,
            ),
          },
        },
      }

    default:
      return state
  }
}
