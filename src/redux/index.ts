import {combineReducers} from 'redux'
import {rssFeedReducer} from './rss_feed'

const reducers = {
  rssFeed: rssFeedReducer,
}

declare global {
  namespace redux {
    /*
      This state has been set to global because it will otherwise trigger circlular imports, because it is used also inside action creators.
      (This shouldn't be issue because almost every component need access to this type-info. Using global type decolators should still be avoided, because
      circular imports can introduce hidden errors that are challenging to catch)

      Either way, this is correct way of defining RootState as globally accessible type:
      just use "redux.RootState" without imports and you are fine.

      >>> REASON FOR DISABLING ESLINT ERROR IN THE NEXT LINE:
      ESlint doesn't know that 'reducers' refer to local reference instead of global, because it is declared here inside global scope.
      <<<
    */
    // eslint-disable-next-line no-undef
    type RootState = import('typesafe-actions').StateType<typeof reducers>
  }
}

export * from './rss_feed'
export const rootReducer = combineReducers(reducers)
