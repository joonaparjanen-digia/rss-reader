/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
const path = require('path')

module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: true,
      },
    }),
  },
  resolver: {
    blockList: [/__.*__/, /.*\.test\..*/, /jest-utils/], // blocks files in that are used only for testing,
    extraNodeModules: {
      //link modules in compiletime
      '#src': path.resolve(`${__dirname}/src`),
      '#classes': path.resolve(`${__dirname}/src/classes`),
      '#screens': path.resolve(`${__dirname}/src/screens`),
      '#navigation': path.resolve(`${__dirname}/src/navigation`),
      '#components': path.resolve(`${__dirname}/src/components`),
      '#styles': path.resolve(`${__dirname}/src/styles`),
      '#redux': path.resolve(`${__dirname}/src/redux`),
      '#types': path.resolve(`${__dirname}/src/types`),
    },
  },
}
