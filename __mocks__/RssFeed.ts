import ch from 'cheerio'
import {RssFeed} from '../src/classes/RSSFeed'
import {XMLContext} from '../src/classes/XMLContext'

const rssFeedMockData = `<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/">
      <channel>
        <language>CHANNEL_LANGUAGE</language>
        <title>CHANNEL_TITLE</title>
        <description>CHANNEL_DESCRIPTION</description>
        <link>CHANNEL_LINK</link>
        <atom:link href="CHANNEL_LINK" rel="self" type="application/rss+xml"/>
        <category>CHANNEL_CATEGORY</category>
        <item>
          <title>TITLE_0</title>
          <link>LINK_0</link>
          <description>DESCRIPTION_0</description>
          <pubDate>PUB_DATE_0</pubDate>
          <guid isPermaLink="false">GUID_0</guid>
          <enclosure url="IMAGE_URL_0" type="image/jpeg" length="0"/>
        </item>
        <item>
          <title>TITLE_1</title>
          <link>LINK_1</link>
          <description>DESCRIPTION_1</description>
          <pubDate>PUB_DATE_1</pubDate>
          <guid isPermaLink="false">GUID_1</guid>
          <enclosure url="IMAGE_URL_1" type="image/jpeg" length="0"/>
        </item>
        <item>
          <title>TITLE_2</title>
          <link>LINK_2</link>
          <description>DESCRIPTION_2</description>
          <pubDate>PUB_DATE_2</pubDate>
          <guid isPermaLink="false">GUID_2</guid>
          <enclosure url="IMAGE_URL_2" type="image/jpeg" length="0"/>
        </item>
        <item>
          <title>TITLE_3</title>
          <link>LINK_3</link>
          <description>DESCRIPTION_3</description>
          <pubDate>PUB_DATE_3</pubDate>
          <guid isPermaLink="false">GUID_3</guid>
          <enclosure url="IMAGE_URL_3" type="image/jpeg" length="0"/>
        </item>
        <item>
          <title>TITLE_4</title>
          <link>LINK_4</link>
          <description>DESCRIPTION_4</description>
          <pubDate>PUB_DATE_4</pubDate>
          <guid isPermaLink="false">GUID_4</guid>
          <enclosure url="IMAGE_URL_4" type="image/jpeg" length="0"/>
        </item>
        <item>
          <title>TITLE_4</title>
          <link>LINK_4</link>
          <description>DESCRIPTION_4</description>
          <pubDate>PUB_DATE_4</pubDate>
          <guid isPermaLink="false">GUID_4</guid>
          <enclosure url="IMAGE_URL_4" type="image/jpeg" length="0"/>
        </item>
      </channel>
    </rss>`

const fetchContext = async (_url: string): Promise<XMLContext> => {
  const $ = ch.load(rssFeedMockData, {
    xmlMode: true,
  })
  return new XMLContext($)
}

jest.spyOn(RssFeed, 'fetchContext').mockImplementation(fetchContext)
